'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as qoFilepath from './quickopen_filepath_utils';
import * as fs from 'fs';
import * as os from 'os';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.speedyOpener', async () => {

        var workingDir = qoFilepath.getWorkingDir();

        var localFilesAndFolders = qoFilepath.listFilesAndDirectories(workingDir);

        var done = false;
        
        while (!done) {
        
            const myQuickPickOptions = {
                placeHolder: workingDir,
                ignoreFocusOut: true
            };

            console.log(workingDir);
            var item = await vscode.window.showQuickPick(localFilesAndFolders, myQuickPickOptions);

            if (!item) {
                // the user cancelled the selection
                console.log('well this is awkward');
                return;
            } else {
                // we have a selection:
                //   if it's a dir, do again the same process for the files in that dir
                //   otherwise just open the file
                const fullPath = workingDir + '/' + item.label;
                console.log(item.label);
                if (item.label === qoFilepath.PARENT_DIR) {
                    workingDir = qoFilepath.getParentDir(workingDir);
                    localFilesAndFolders = qoFilepath.listFilesAndDirectories(workingDir);
                } else if (item.label === qoFilepath.ABS_PATH) {
                    const _absInput = await vscode.window.showInputBox({placeHolder: "enter path here"});
                    const absInput = _absInput ? _absInput.replace(/^~/, os.homedir()) : '';
                    try {
                        if (fs.lstatSync(absInput).isDirectory()) {
                            workingDir = absInput;
                            localFilesAndFolders = qoFilepath.listFilesAndDirectories(workingDir);
                        } else if (fs.lstatSync(absInput).isFile()) {
                            vscode.window.showTextDocument(vscode.Uri.file(absInput));
                        }
                    } catch (err) {
                        console.log(err);
                        vscode.window.showErrorMessage('Could not open ' + absInput);
                    }
                } else if (qoFilepath.isDirectory(fullPath)) {
                    workingDir = fullPath;
                    localFilesAndFolders = qoFilepath.listFilesAndDirectories(workingDir);
                } else {
                    // open the selected file
                    try {
                        vscode.window.showTextDocument(vscode.Uri.file(workingDir + '/' + item.label));
                        done = true;
                    } catch (err) {
                        console.log(err);
                    }
                }
            }
        }
    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}
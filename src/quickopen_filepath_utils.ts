/* 
 * quickopen_filepath_utils.js
 * 
 * Contains a number of methods to manipulate and process file paths
 * 
*/

import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import { QuickOpenElement } from './quickopen';

export const PARENT_DIR = '(Parent Directory)';
export const ABS_PATH = '(Jump to...)';

export function parseWorkspaceFolders () {
    // Returns the list of workspace folders, or undefined if no workspace is open
    var toReturn = '';

    // query VSCode to find out open workspace's folders
    var wsFolders = vscode.workspace.workspaceFolders;
    
    // check for no workspace open
    if (wsFolders !== undefined) {
        toReturn = wsFolders.toString();
    } else {
        toReturn = 'undefined';
    }

    return toReturn;
}

export function parseOpenFilePath () {
    // Returns the list of workspace folders, or undefined if no workspace is open
    var toReturn = '';

    // query VSCode to find out open file name
    try {
        var openedDocument = vscode.window.activeTextEditor!.document;
        toReturn = openedDocument.fileName;
    } catch(err) {
        toReturn = 'undefined';
    }

    return toReturn;
}

export function getDefaultDirForPlatform () {
    return require('os').homedir();

}

export function getWorkingDir (editorPathHarness = "", workspaceRootHarness = "") {

    var editorPath;
    var workspaceRoot;
    var workingDir;

    // Test harnesses
    if (editorPathHarness !== "") {
        editorPath = editorPathHarness;
    } else {
        editorPath = parseOpenFilePath();
    }

    if (workspaceRootHarness !== "") {
        workspaceRoot = workspaceRootHarness;
    } else {
        workspaceRoot = vscode.workspace.rootPath ? vscode.workspace.rootPath : 'undefined';
    }

    // selects and returns the working directory in this priority:
    // 1_ if an editor is open, selects the directory containing the file being edited
    // 2_ if a workspace is open, selects the root of the workspace
    // 3_ if all else fails, selects C:\ on Windows and ~/ on *nix
    if (!(editorPath === 'undefined')) {
        workingDir = path.dirname(editorPath);
    } else if (!(workspaceRoot === 'undefined')) {
        workingDir = workspaceRoot;
    } else {
        workingDir = getDefaultDirForPlatform();
    }

    return workingDir;
}

export function listFilesAndDirectories (path: string) {
    var returnArray: QuickOpenElement[] = [];

    const dirUpLabel: QuickOpenElement = { label: PARENT_DIR };
    returnArray.push(dirUpLabel);

    const absPathLabel: QuickOpenElement = { label: ABS_PATH };
    returnArray.push(absPathLabel);

    fs.readdirSync(path).forEach(file => {
        const newFile: QuickOpenElement = { label: file };
        returnArray.push(newFile);
    });

    return returnArray;
}

export function isDirectory (path: string) {
    return fs.lstatSync(path).isDirectory();
}

export function getParentDir (file: string) {
    return path.dirname(file);
}
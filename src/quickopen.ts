import { QuickPickItem } from 'vscode';

export class QuickOpenElement implements QuickPickItem {
    label: string;

    constructor(label: string) {
        this.label = label;
    }
}
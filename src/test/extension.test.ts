//
// Note: This example test is leveraging the Mocha test framework.
// Please refer to their documentation on https://mochajs.org/ for help.
//

// The module 'assert' provides assertion methods from node
import * as assert from 'assert';

import * as vscode from 'vscode';
import * as qoFilepath from '../quickopen_filepath_utils';
import * as path from 'path';

// Defines a Mocha test suite to group tests of similar kind together
suite("Infra Alive Suite", function() {

    // Defines a Mocha unit test
    test("it can pass", function() {
        assert.equal(-1, [1, 2, 3].indexOf(5));
        assert.equal( 0, [1, 2, 3].indexOf(1));
    });
});

// Defines a Mocha test suite to group tests of similar kind together
suite("QuickOpen FilePath Utils Suite", function() {

    test("parseWorkspaceFolders() can handle undefined", function() {
        var result = qoFilepath.parseWorkspaceFolders();
        assert(result, 'undefined');
    });

    // FIXME: needs tests for proper parseWorkspaceFolders() behaviour -
    // how do I change workspace from the test environment?

    test("parseOpenFilePath() can handle undefined", function() {
        var result = qoFilepath.parseOpenFilePath();
        assert(result, 'undefined');
    });

    test("parseOpenFilePath() can detect an open file's path", function() {
        // open a new editor so that we can actually test the function...

        // inception
        const path = vscode.workspace.rootPath + '/extension.test.js';
        const pathUri = vscode.Uri.file(path);

        var result = '';

        // showTextDocument expects Uri
        return vscode.window.showTextDocument(pathUri).then( () => {
            
            // did the editor open just fine?
            const active = vscode.window.activeTextEditor;
            assert.ok(active);

            // check that parseOpenFilePath detects what we opened
            result = qoFilepath.parseOpenFilePath();
            assert.equal(result, path);

        });
        
    });

    test("getWorkingDir() returns the default folder if no file or workspace is open", function () {
        const editorPathArray = ['undefined'];
        const workspaceRootArray = ['undefined'];

        const filesystemRoot = '~/';

        // hijack pathHarness to inject *nix paths in case we're running on Windows
        for (var i in editorPathArray) {
            var editorPath = editorPathArray[i];
            for (var j in workspaceRootArray) {
                var workspaceRoot = workspaceRootArray[j];
                assert.equal(qoFilepath.getWorkingDir(editorPath, workspaceRoot), filesystemRoot);
            }
        }
    });

    test("getWorkingDir() returns the workspace root if no file is open but a workspace is", function () {
        const editorPathArray = ['undefined'];
        const workspaceRootArray = ['~/', '/home/my_username/Documents', '/media/6AUF_BLSM/', '/mnt/c/Program Files'];

        // hijack pathHarness to inject *nix paths in case we're running on Windows
        for (var i in editorPathArray) {
            var editorPath = editorPathArray[i];
            for (var j in workspaceRootArray) {
                var workspaceRoot = workspaceRootArray[j];
                assert.equal(qoFilepath.getWorkingDir(editorPath, workspaceRoot), workspaceRoot);
            }
        }
    });

    test("getWorkingDir() returns the edited file's base folder if no file is open but a workspace is", function () {
        const editorPathArray = ['~/.bashrc', '/home/my_username/Documents/configs.ini', '/media/6AUF_BLSM/file.with.lotta.dots', '/mnt/c/Program Files/hey_buddy.py'];
        const workspaceRootArray = ['~/', '/home/my_username/Documents', '/media/6AUF_BLSM/', '/mnt/c/Program Files'];

        // hijack pathHarness to inject *nix paths in case we're running on Windows
        for (var i in editorPathArray) {
            var editorPath = editorPathArray[i];
            for (var j in workspaceRootArray) {
                var workspaceRoot = workspaceRootArray[j];
                assert.equal(qoFilepath.getWorkingDir(editorPath, workspaceRoot), path.dirname(editorPath));
            }
        }
    });

    test("listFilesAndDirectories() returns expected elements for test vectors", function () {
        assert.deepEqual(qoFilepath.listFilesAndDirectories("src/test/listFilesAndDirectories"),   [{ label: 'Parent Directory' }, { label: '0' }, { label: '1' }, { label: '2' }]);
        assert.deepEqual(qoFilepath.listFilesAndDirectories("src/test/listFilesAndDirectories/0"), [{ label: 'Parent Directory' }]);
        assert.deepEqual(qoFilepath.listFilesAndDirectories("src/test/listFilesAndDirectories/1"), [{ label: 'Parent Directory' }, { label: 'single_file.txt' }]);
        assert.deepEqual(qoFilepath.listFilesAndDirectories("src/test/listFilesAndDirectories/2"), [{ label: 'Parent Directory' }, { label: '__file1.c' }, { label: 'file0.c' }]);
    });
});
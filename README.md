# SpeedyOpener for VSCode

SpeedyOpener allows the user to replace the standard behaviour for "Open file..." with a smart navigator shown natively in Visual
Studio Code.

## Features

SpeedyOpener currently offers the following features:

* adds the "SpeedyOpener" command in the VSCode Command Palette
  * shows a prompt that allows filesystem navigation with fuzzy completion
  * files are opened in new tabs

## Requirements

None! Should work out of the box.

## Extension Settings

There is no configuration for SpeedyOpener as of the latest release.

## Known Issues

SpeedyOpener is currently in alpha!

* While a small subset of unit tests is actively maintained, SpeedyOpener is far from being thoroughly tested in real world usecases.
* SpeedyOpener has been developed and tested on macOS. The code should have been written in a portable way, but so far no 
  testing has happened on Windows/Linux installations.

## Release Notes

### 0.2.0

* SpeedyOpener is now invoked using Ctrl+O (or Cmd+O on macOS)
* Tracks the current path in the input box
* Offers options to go up a directory or jump to an absolute path

### 0.1.0

Alpha release of SpeedyOpener.
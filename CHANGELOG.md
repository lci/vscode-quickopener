# Changelog

## 0.2.0

* SpeedyOpener is now invoked using Ctrl+O (or Cmd+O on macOS)
* Tracks the current path in the input box
* Offers options to go up a directory or jump to an absolute path

## 0.1.0

Alpha release of SpeedyOpener.